from django.urls import path,include
from django.conf.urls.static import static
from django.conf import settings
from . import views


urlpatterns = [
    path('search', views.search),
    path('id=(?P<ids>[0-9]+)', views.index, name='property'),
    path('book_form/id=(?P<ids>[0-9]+)', views.book_form, name='book_form'),
    path('book/id=(?P<ids>[0-9]+)$', views.book, name='book'),
    path('<int:id>/',include('comment.urls'),name='comment'),
    #path('comment/<int:id>',views.comment_form,name='comment_form'),
    path('post_form/uid=(?P<ids>[0-9]+)', views.post_form, name='post_form'),
    path('post/uid=(?P<ids>[0-9]+)', views.post, name='post'),
              ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)