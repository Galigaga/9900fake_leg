from django.db import models

import datetime

class FakeLeg(models.Model):
	stateChoice = (
		('NSW', 'New South Wales'),
		('VIC', 'Victoria'),
		('QLD', 'Queensland'),
		('TAS', 'Tasmania'),
		('SA', 'South Australia'),
		('WA', 'Western Australia'),
		('ACT', 'Australian Capital Territory'),
		('NT', 'Northern Territory'),
	)
	UID = models.IntegerField()
	suburb = models.CharField(max_length=50)
	state = models.CharField(max_length=3, choices=stateChoice)
	st_name = models.CharField(max_length=20, null=True)
	st_number = models.IntegerField(null=True)
	tenant_num = models.IntegerField()
	price = models.IntegerField()
	house_type = models.CharField(max_length=20, null=True)
	pet = models.BooleanField(null=True)
	wifi = models.BooleanField(null=True)
	kitchen = models.BooleanField(null=True)
	laundry = models.BooleanField(null=True)
	park_lot = models.BooleanField(null=True)
	brief_intro = models.CharField(max_length=500, null=True)
	img_url = models.ImageField(upload_to='img', null=True)

class Date(models.Model):
	PID = models.ForeignKey('FakeLeg', on_delete=models.CASCADE)
	booked_date = models.IntegerField()
	
class Guest(models.Model):
	stateChoice = (
		('NSW', 'New South Wales'),
		('VIC', 'Victoria'),
		('QLD', 'Queensland'),
		('TAS', 'Tasmania'),
		('SA', 'South Australia'),
		('WA', 'Western Australia'),
		('ACT', 'Australian Capital Territory'),
		('NT', 'Northern Territory'),
	)
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	mobile = models.BigIntegerField()
	email = models.EmailField()
	start_date = models.IntegerField()
	duration = models.IntegerField()
	ID_number = models.IntegerField()
	st_number = models.IntegerField()
	st_name = models.CharField(max_length=20)
	suburb = models.CharField(max_length=50)
	state = models.CharField(max_length=3, choices=stateChoice)
	card_number = models.BigIntegerField()
	PID = models.ForeignKey('FakeLeg', on_delete=models.CASCADE)