from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from .models import FakeLeg, Date, Guest
from django.core.mail import send_mail

# 表单
def search_form(request):
	return render_to_response('search_form.html')
 
# 接收请求数据
def search(request):  
	request.encoding='utf-8'
	response = ""
	response1 = ""
	dic = {}
	if 'state' in request.GET and 'suburb' in request.GET and 'person' in request.GET and 'price1' in request.GET and 'price2' in request.GET:
		b = 0
		c = 100000
		if request.GET['price1'] != '':
			b = int(request.GET['price1'])
		if request.GET['price2'] != '':
			c = int(request.GET['price2'])
		if request.GET['state'] != '':
			dic['state'] = request.GET['state']
		if request.GET['suburb'] != '' :
			dic['suburb'] = request.GET['suburb']
		if request.GET['person'] != '' :
			a = int(request.GET['person'])
			dic['tenant_num'] = a
		if dic != {}:
			li = FakeLeg.objects.filter(**dic)
		else:
			li = FakeLeg.objects.all()
#		i = 1
		res = []
		for var in li:
			if var.price < b or var.price > c:
				continue
			res.append(var)
#			response1 += str(i) + "." + "User ID: " + str(var.UID) + "<br>Suburb: " + var.suburb + "<br>State: " + var.state +"<br>Guest Num: " + str(var.tenant_num) + "<br>Price: $" + str(var.price) + "/night" + "<br>" + "<br>"
#			i += 1
		response = response1
		return render(request, "search_result.html", {'res': res})
	else:
		message = '你提交了空表单'
	return HttpResponse(message)

def index(request, ids):
	results = FakeLeg.objects.get(id=ids)
	dates = Date.objects.filter(PID_id=ids)
	return render(request, "property.html", {'results': results, 'dates': dates})

def book_form(request, ids):
	return render(request, 'book_form.html', {'id': ids})
	
def book(request, ids):
	request.encoding='utf-8'
	if request.method == 'GET':
		duration = int(request.GET['e_date']) - int(request.GET['s_date'])
		guest = Guest(first_name=request.GET['f_name'], last_name=request.GET['l_name'], mobile=int(request.GET['mobile']), email=request.GET['email'], start_date=int(request.GET['s_date']), duration=duration, ID_number=int(request.GET['id_num']), st_number=int(request.GET['st_num']), st_name=request.GET['st_name'], suburb=request.GET['suburb'], state=request.GET['state'], card_number=int(request.GET['card_num']), PID_id=ids)
		guest.save()
		current_date = int(request.GET['s_date'])
		for i in range(duration):
			date = Date(booked_date=current_date, PID_id=ids)
			date.save()
			current_date += 1
		house = FakeLeg.objects.get(id=ids)
		# send_mail('Booking Successfully', 'New Booking', '513339224@qq.com', [request.GET['email']], fail_silently=False)
		return HttpResponse("<p> Booking Successfully! </p>")
"""
#for comment
def comment_form(request, ids):
	return render(request,'comment.html',{'id':ids})
"""


def post_form(request, ids):
	return render(request, 'post_form.html', {'id': ids})
	
def post(request, ids):
	request.encoding='utf-8'
	if request.method == 'POST':
		state = request.POST.get('state')
		print(state)
		suburb = request.POST.get('suburb')
		print(suburb)
		print(ids)
#		st_number = request.POST.get('st_num', None)
#		st_name = request.POST.get('st_name', None)
#		house_type = request.POST.get('house_type', None)
#		pet = request.POST.get('pet', None)
#		wifi = request.POST.get('wifi', None)
#		kitchen = request.POST.get('kitchen', None)
#		laundry = request.POST.get('laundry', None)
#		park_lot = request.POST.get('park_lot', None)
#		tenant_num = request.POST.get('person', None)
#		price = request.POST.get('price', None)
#		brief_intro = request.POST.get('brief_intro', None)
#		img_url = request.FILES.get('img')
#		newp = FakeLeg(UID=ids, state=state, suburb=suburb, tenant_num=tenant_num, price=price)
#		newp.save()
	return HttpResponse("<p> Posting Successfully! </p>")