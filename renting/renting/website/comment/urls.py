from django.urls import path,include
from . import views
from user_info import views as user_info_views
urlpatterns = [
            path('comment/', views.comment, name='comment'),
            path('login/',user_info_views.login,name='login'),
            #path('user_info/',include('user_info.urls')),
            path('update_comment/',views.update_comment,name='update_comment'),
]


