from django.shortcuts import get_object_or_404,render,redirect
from django.urls import reverse
from property.models import FakeLeg, Date, Guest
from .models import Comment
from django.contrib.contenttypes.models import ContentType


# Create your views here.

def comment(request,id):
    property = get_object_or_404(FakeLeg,id=id)
    property_content_type =ContentType.objects.get_for_model(FakeLeg)
    comments = Comment.objects.filter(content_type=property_content_type,object_id = id,parent=None)


    context = {}
    context['property'] = property
    context['comments'] = comments

    response = render(request,'comment.html',context)
    return response

def update_comment(request):
    referer = request.META.get('HTTP_REFERER', reverse('home'))
    user = request.user
    if not user.is_authenticated:
        return render(request, 'error.html', {'message': 'you should login ','redirect_to':referer})

    text = request.POST.get('text','').strip()
    #if comment is empty, return an error
    if text == '':
        return render(request,'error.html',{'message': 'Comment content is empty','redirect_to':referer})

    try:

        content_type = request.POST.get('content_type','')
        object_id = int(request.POST.get('object_id','0'))
        model_class = ContentType.objects.get(model=content_type).model_class()
        model_obj = model_class.objects.get(id=object_id)

    except Exception as e:
        return render(request, 'error.html', {'message': 'Comment object does not exist','redirect_to':referer})
    #if all requirements is obeyed, then generate the model instance
    comment = Comment()
    comment.user = user
    comment.text = text
    comment.content_object = model_obj
    comment.save()

    return redirect(referer)
