week 2

Group formed and topic 1 was chosen as the project topic.
In group meeting, we confirmed the methodology that is going to be used in this project.
I wrote a proposal syllbus which summarizes the content that should be included in proposal.
I also wrote the Project Background/Domain and Existing System and Drawbacks part of proposal.

week 3

On Tuesday we had a group meeting to confirm about the architecture of project system.
However, we had problem of the implementation of project 1. Therefore, we swap to project 3 with 
agreement of all team members.
On Thursday, we had another group meeting to discuss about aim, epics and progress plan of 
topic 3, and also shared throughts about project architecture and layout. Afterwards, we wrote proposal 
together and I was mainly responsible for part of outlining the drawbacks of current system and brainstorming 
some solutions.

week 4
In this week, we started to implement specific parts of project architecture. I mainly responsible 
for the back end code implementation and database design and implementation. I built a database and 
a basic table which is able to record information including address, number of guests, and price etc. 
At the same time, I accessed built database via python API and inserted some sample data. 
I also did some sql query using python API and return the query result correctly.